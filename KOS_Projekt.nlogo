globals[
  home-time
  station-time
  total-watts-home
  total-watts-stations



]
breed[stations station]
breed[residences residence]
breed[cars car]
cars-own
[
  car-home
  preference
  typeofCar


  state
  km
  time
  near-station

  percentage
  max-capacity
  capacity
  consumption
  max-charge
  fast-charge
  timeonstation
]
residences-own[
  station?
  watts
]
stations-own[
  fast-charge?
  watts
]
to go
  ;one year
  if ticks > 525600 [stop]
  charge
  direction
  move
  waiting
  tick ;; 1 hour
end
to waiting
  ask cars with [state = "waiting"][
  set timeonstation (timeonstation + 1)
    if(timeonstation = waiting-time )[
    set state "charging"
    ]


  ]
end
to charge
  ;Emergency charging
  ask cars with [state = "emergency"][
    set capacity (capacity + (2.3 / 60))
    set percentage ((capacity / max-capacity) * 100)
    if(percentage >= 10)
    [
      set state "go"
    ]

  ]



  ask cars with [state = "charging"][

    let nears-x [pxcor] of near-station
    let nears-y [pycor] of near-station
    let chargeMode nobody
    let chargedWatts 0
    let chargeHome false
    let full false
    let new-capacity 0
    ; choose charger
    ifelse(preference = 1 and (fast-charge > 0))[
      ask stations with [xcor = nears-x and ycor = nears-y]
      [
        set chargeMode  fast-charge?
      ]
    ][
      ask residences with [xcor = nears-x and ycor = nears-y]
      [
        set chargeMode  false
        set chargeHome true
      ]

    ]

    ;fast charge
    ifelse (chargeMode = true)
    [
      set timeonstation (timeonstation + 1)
      set new-capacity  (capacity + (fast-charge / 60))
      ifelse( new-capacity <= max-capacity)[
        set capacity new-capacity
        set chargedWatts (fast-charge / 60)
        set percentage ((capacity / max-capacity) * 100)
      ][
        set percentage 100
        set chargedWatts (max-capacity - capacity)
        set capacity max-capacity
        set near-station 0
        set timeonstation 0
        set state "go"
      ]



    ]
    [
      ;normal charging
      set timeonstation (timeonstation + 1)
      ifelse(chargeHome = true) [
        set new-capacity  (capacity + (max-charge / 60))
      ][
        set new-capacity  (capacity + (2.3 / 60))
      ]


      ifelse( new-capacity < max-capacity)[
        set capacity new-capacity
        ifelse(chargeHome = true)[
          set chargedWatts (2.3 / 60)
        ][
          set chargedWatts (max-charge / 60)
        ]

        set percentage ((capacity / max-capacity) * 100)
      ][
        set percentage 100
        set chargedWatts (max-capacity - capacity)
        set capacity max-capacity
        set near-station 0
        set timeonstation 0
        set state "go"

      ]


    ]




    ifelse(preference = 1)[
      ask stations with [xcor = nears-x and ycor = nears-y]
      [
        set watts (watts + chargedWatts)
        set total-watts-stations (total-watts-stations + watts)

      ]
    ][
      ask residences with [xcor = nears-x and ycor = nears-y]
      [

        set watts (watts + chargedWatts)
        set total-watts-home (total-watts-home + watts)
      ]

    ]

  ]

end
to direction
  ask cars with [pcolor = gray]
  [

    ifelse(percentage <= 50 )
    [
      let fstation patch 0 0
      ifelse (preference = 0)
      [
        if(car-home = 0)
        [
          set fstation (patch 22 0)
        ]
        if(car-home = 1)
        [
          set fstation (patch 22 -22)
        ]
        if(car-home = 2)
        [
          set fstation (patch 8 22)
        ]
        if(car-home = 3)
        [
          set fstation (patch -22 22)
        ]
      ]
      [
        set fstation min-one-of patches with [pcolor = orange][distance myself]
      ]

      set near-station fstation
      let nears-x [pxcor] of near-station
      let nears-y [pycor] of near-station

      ;align x

      ifelse (((pxcor + 1) = nears-x) or ((pxcor - 1) = nears-x))[
        ifelse(pycor > nears-y)
        [
          set heading 180
        ]
        [
          set heading 0
        ]

      ][

        if(pxcor > nears-x)
        [
          set heading 270
        ]
        if(pxcor < nears-x)[
          set heading 90
        ]


      ]


    ][
      ifelse(random 90 >= 45)
      [
        left 90
      ]
      [
        if ( random 10 < 5)[
          left 180
        ]
      ]

    ]


  ]


  ask cars with [pcolor = black]
  [


    if (percentage <= 50 and near-station = 0)
    [
      let cross min-one-of patches with [pcolor = gray][distance myself]
      face cross
    ]

    if (near-station != 0)
    [


      ifelse (preference = 0)
      [

        let hx [pxcor] of near-station
        let hy  [pycor] of near-station

        if(((pxcor + 1) = hx) or ((pxcor - 1) = hx))

        [

          if any? residences-on neighbors4[
            set state "charging"

          ]

        ]



      ]

      [
        if any? stations-on neighbors4[
          let wait_prob random-float 1
          if(timeonstation > 0 and (state != "waiting"))[
             set timeonstation 0
            set wait_prob 1.2
          ]
          ifelse(wait_prob < probability-of-waiting)
          [
            set state "waiting"][
            set state "charging"
          ]

        ]
      ]

    ]



  ]


end
to move
  ask cars[
    ; no capacity left , emergency charging in neighbour house
    if (capacity <= 0 ) [
      set state  "emergency"
    ]

    ifelse([pcolor] of patch-ahead 1 = black)
    [



      if (state = "go")[
        fd 1
        set km (km + (average-speed / 60))
        set time (time + 1)
      ]





      if ((time mod 60) = 0)
        [
          set capacity  (capacity - consumption)
          set percentage ((capacity / max-capacity) * 100)
      ]

    ][
      if([pcolor] of patch-ahead 1 = gray)
      [

        if (state = "go")[
          fd 1
          set km (km + (average-speed / 60))
          set time (time + 1)
        ]
        if ((time mod 60) = 0)
        [
          set capacity  (capacity - consumption)
          set percentage ((capacity / max-capacity) * 100)
        ]
      ]


    ]
  ]
end

to setup
  clear-all
  reset-ticks
  layout
  ask cars [
    set timeonstation 0
  ]
  set  waiting-time 40
end
to layout
  ask patches [
    set pcolor green
  ]
  ask patches with [pxcor >= -29 and pxcor <= 29 and pycor = -29]
    [set pcolor black]
  ask patches with [pxcor >= -29 and pxcor <= 29 and pycor = 21]
    [set pcolor black]
  ask patches with [pxcor >= -29 and pxcor <= 29 and pycor = 7]
    [set pcolor black]

  ask patches with [pxcor >= -29 and pxcor <= 29 and pycor = -7]
    [set pcolor black]

  ask patches with [pxcor >= -29 and pxcor <= 29 and pycor = -21]
    [set pcolor black]
  ask patches with [pxcor >= -29 and pxcor <= 29 and pycor = 29]
  [set pcolor black]





  ask patches with [pycor >= -29 and pycor <= 29 and pxcor = -29]
    [set pcolor black]

  ask patches with [pycor >= -29 and pycor <= 29 and pxcor = -21]
    [set pcolor black]
  ask patches with [pycor >= -29 and pycor <= 29 and pxcor = -7]
    [set pcolor black]
  ask patches with [pycor >= -29 and pycor <= 29 and pxcor = 7]
    [set pcolor black]
  ask patches with [pycor >= -29 and pycor <= 29 and pxcor = 21]
    [set pcolor black]
  ask patches with [pycor >= -29 and pycor <= 29 and pxcor = 29]
    [set pcolor black]
  ;cross
  ask patches with [pxcor = -29 and (pycor = 29 or pycor = 21 or pycor = 7 or pycor = -7 or pycor = -21 or pycor = -29)]
    [set pcolor gray]
  ask patches with [pxcor = -21 and (pycor = 29 or pycor = 21 or pycor = 7 or pycor = -7 or pycor = -21 or pycor = -29)]
  [set pcolor gray]
  ask patches with [pxcor = -7 and (pycor = 29 or pycor = 21 or pycor = 7 or pycor = -7 or pycor = -21 or pycor = -29)]
  [set pcolor gray]
  ask patches with [pxcor = 7 and (pycor = 29 or pycor = 21 or pycor = 7 or pycor = -7 or pycor = -21 or pycor = -29)]
    [set pcolor gray]
  ask patches with [pxcor = 21 and (pycor = 29 or pycor = 21 or pycor = 7 or pycor = -7 or pycor = -21 or pycor = -29)]
  [set pcolor gray]
  ask patches with [pxcor = 29 and (pycor = 29 or pycor = 21 or pycor = 7 or pycor = -7 or pycor = -21 or pycor = -29)]
  [set pcolor gray]

  ;home
  ;0
  ask patches with [pycor = 0 and pxcor = 22 ][
    sprout-residences 1 [
      set shape "house"
      set color red
      set station? true
      set size 1

    ]
  ]
  ;1
  ask patches with [pycor = -22 and pxcor = 22 ][
    sprout-residences 1 [
      set shape "house"
      set color red
      set station? false
      set size 1

    ]
  ]
  ;2
  ask patches with [pycor = 22 and pxcor = 8 ][
    sprout-residences 1 [
      set shape "house"
      set color red
      set station? true
      set size 1

    ]
  ]
  ;3
  ask patches with [pycor = 22 and pxcor = -22 ][
    sprout-residences 1 [
      set shape "house"
      set color red
      set station? false
      set size 1

    ]
  ]


  ;cars
  cars-0
  cars-1
  cars-2
  cars-3
  create-stat






end
to create-stat

  ;stations
  let stationsnum 0
  let i -28
  while [i <= 28]
  [
    let num random 20
    if (i != 21 and i != 7 and i != -7 and i != -21 and i != -29 )
    [
      if ( (num >= 10 and num <= 14) and stationsnum <= max-station-road)[
        ask patches with [pxcor = 6 and pycor = i][
          set pcolor orange
          sprout-stations 1 [
            set shape "square"
            set color orange
            set fast-charge? true
            set watts 0
          ]

          if ((i + 6) <= 29 )
          [set i (i + 6)]
          set stationsnum stationsnum + 1
        ]
      ]

    ]


    set i (i + 1)
  ]

  set stationsnum 0
  set i -28
  while [i <= 28]
  [
    let num random 20
    if (i != 21 and i != 7 and i != -7 and i != -21 and i != -29 )
    [
      if ( (num >= 10 and num <= 14) and stationsnum <= max-station-road)[
        ask patches with [pxcor = -6 and pycor = i][
          set pcolor orange
          sprout-stations 1 [
            set shape "square"
            set color orange
            set fast-charge? false
            set watts 0
          ]
          set i (i + 6)
          set stationsnum stationsnum + 1
        ]
      ]

    ]


    set i (i + 1)
  ]

  set stationsnum 0
  set i -28
  while [i <= 28]
  [
    let num random 20
    if (i != 21 and i != 7 and i != -7 and i != -21 and i != -29  )
    [
      if ( (num >= 10 and num <= 14) and stationsnum <= max-station-road)[
        ask patches with [pxcor = -22 and pycor = i][
          set pcolor orange
          sprout-stations 1 [
            set shape "square"
            set color orange
            set fast-charge? true
            set watts 0
          ]
          set i (i + 6)
          set stationsnum stationsnum + 1
        ]
      ]

    ]


    set i (i + 1)
  ]

  set stationsnum 0
  set i -28
  while [i <= 28]
  [
    let num random 20
    if (i != 21 and i != 7 and i != -7 and i != -21 and i != -29 )
    [
      if ( (num >= 10 and num <= 14) and stationsnum <= max-station-road)[
        ask patches with [pxcor = 22 and pycor = i][
          set pcolor orange
          sprout-stations 1 [
            set shape "square"
            set color orange
            set fast-charge? false
            set watts 0
          ]
          set i (i + 6)
          set stationsnum stationsnum + 1
        ]
      ]

    ]


    set i (i + 1)
  ]

  set stationsnum 0
  set i -28
  while [i <= 28]
  [
    let num random 20
    if (i != 21 and i != 7 and i != -7 and i != -21 and i != -29 )
    [
      if ( (num >= 10 and num <= 14) and stationsnum <= max-station-road)[
        ask patches with [pxcor = 28 and pycor = i][
          set pcolor orange
          sprout-stations 1 [
            set shape "square"
            set color orange
            set fast-charge? true
            set watts 0
          ]
          set i (i + 6)
          set stationsnum stationsnum + 1
        ]
      ]

    ]


    set i (i + 1)
  ]
  set stationsnum 0
  set i -28
  while [i <= 28]
  [
    let num random 20
    if (i != 21 and i != 7 and i != -7 and i != -21 and i != -29 )
    [
      if ( (num >= 10 and num <= 14) and stationsnum <= max-station-road)[
        ask patches with [pxcor = -28 and pycor = i][
          set pcolor orange
          sprout-stations 1 [
            set shape "square"
            set color orange
            set fast-charge? false
            set watts 0
          ]
          set i (i + 6)
          set stationsnum stationsnum + 1
        ]
      ]

    ]


    set i (i + 1)
  ]

end
to cars-0
  let number  floor (number-of-cars-residence / 3)

  ask patch 21 0[

    let halfn floor(number / 2)
    let rest  number - halfn
    sprout-cars halfn[
      set shape "car"
      set color red
      set car-home 0
      set preference 1
      set typeofCar "sport"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 95
      set capacity 95
      set consumption 18.6
      set max-charge 16.5
      set fast-charge 200
      face patch 21 30

    ]
    sprout-cars halfn[
      set shape "car"
      set color red
      set car-home 0
      set preference 0
      set typeofCar "sport"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 95
      set capacity 95
      set consumption 18.6
      set max-charge 16.5
      set fast-charge 200
      face patch 21 30

    ]




    sprout-cars halfn[
      set shape "car"
      set color blue
      set car-home 0
      set preference 0
      set typeofCar "small"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 52
      set capacity 52
      set consumption 16.5
      set max-charge 22
      set fast-charge 46
      face patch 21 30

    ]
    sprout-cars rest[
      set shape "car"
      set color blue
      set car-home 0
      set preference 1
      set typeofCar "small"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 52
      set capacity 52
      set consumption 16.5
      set max-charge 22
      set fast-charge 46
      face patch 21 30

    ]


    set number  (number-of-cars-residence - 2 * (floor (number-of-cars-residence / 3)))

    set halfn floor(number / 2)
    set rest number - halfn
    sprout-cars halfn[
      set shape "car"
      set color white
      set car-home 0
      set preference 0
      set typeofCar "pickup"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 31
      set capacity 31
      set consumption 18.8
      set max-charge 7.4
      set fast-charge 0
      face patch 21 30
    ]
    sprout-cars rest[
      set shape "car"
      set color white
      set car-home 0
      set preference 1
      set typeofCar "pickup"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 31
      set capacity 31
      set consumption 18.8
      set max-charge 7.4
      set fast-charge 0
      face patch 21 30
    ]
  ]


end
to cars-1

  let number  floor (number-of-cars-residence / 3)
  let halfn floor(number / 2)
  let rest  number - halfn
  ask patch 22 -21[
    sprout-cars halfn[
      set shape "car"
      set color red
      set car-home 1
      set preference 1
      set typeofCar "sport"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 95
      set capacity 95
      set consumption 18.6
      set max-charge 16.5
      set fast-charge 200
      face patch -30 -21

    ]

    sprout-cars rest[
      set shape "car"
      set color red
      set car-home 1
      set preference 0
      set typeofCar "sport"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 95
      set capacity 95
      set consumption 18.6
      set max-charge 16.5
      set fast-charge 200
      face patch -30 -21

    ]

    sprout-cars halfn[
      set shape "car"
      set color blue
      set car-home 1
      set preference 0
      set typeofCar "small"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 52
      set capacity 52
      set consumption 16.5
      set max-charge 22
      set fast-charge 46
      face patch -30 -21

    ]
    sprout-cars rest[
      set shape "car"
      set color blue
      set car-home 1
      set preference 1
      set typeofCar "small"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 52
      set capacity 52
      set consumption 16.5
      set max-charge 22
      set fast-charge 46
      face patch -30 -21

    ]
    set number  (number-of-cars-residence - 2 * (floor (number-of-cars-residence / 3)))
    sprout-cars number[
      set shape "car"
      set color white
      set car-home 1
      set preference 0
      set typeofCar "pickup"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 31
      set capacity 31
      set consumption 18.8
      set max-charge 7.4
      set fast-charge 0
      face patch -30 -21

    ]
  ]
end
to cars-2
  let number  floor (number-of-cars-residence / 3)
  ask patch 7 22[
    sprout-cars number[
      set shape "car"
      set color red
      set car-home 2
      set preference 1
      set typeofCar "sport"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 95
      set capacity 95
      set consumption 18.6
      set max-charge 16.5
      set fast-charge 200
      face patch 7 -30
    ]
    sprout-cars number[
      set shape "car"
      set color blue
      set car-home 2
      set preference 0
      set typeofCar "small"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 52
      set capacity 52
      set consumption 16.5
      set max-charge 22
      set fast-charge 46
      face patch 7 -30
    ]
    set number  (number-of-cars-residence - 2 * (floor (number-of-cars-residence / 3)))
    sprout-cars number[
      set shape "car"
      set color white
      set car-home 2
      set preference 0
      set typeofCar "pickup"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 31
      set capacity 31
      set consumption 18.8
      set max-charge 7.4
      set fast-charge 0
      face patch 7 -30

    ]
  ]
end
to cars-3
  let number  floor (number-of-cars-residence / 3)
  ask patch -22 21[
    sprout-cars number[
      set shape "car"
      set color red
      set car-home 3
      set preference 1
      set typeofCar "sport"
      set size 1


      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 95
      set capacity 95
      set consumption 18.6
      set max-charge 16.5
      set fast-charge 200
      face patch 30 21
    ]
    sprout-cars number[
      set shape "car"
      set color blue
      set car-home 3
      set preference 0
      set typeofCar "small"
      set size 1

      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 52
      set capacity 52
      set consumption 16.5
      set max-charge 22
      set fast-charge 46
      face patch 30 21

    ]
    set number  (number-of-cars-residence - 2 * (floor (number-of-cars-residence / 3)))
    sprout-cars number[
      set shape "car"
      set color white
      set car-home 3
      set preference 0
      set typeofCar "pickup"
      set size 1

      set state "go"
      set km 0
      set time 0

      set percentage 100
      set max-capacity 31
      set capacity 31
      set consumption 18.8
      set max-charge 7.4
      set fast-charge 0
      face patch 30 21

    ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
210
10
1141
942
-1
-1
13.0
1
10
1
1
1
0
1
1
1
-35
35
-35
35
0
0
1
ticks
30.0

BUTTON
124
86
187
119
go
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
122
47
187
82
go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
0
10
200
43
number-of-cars-residence
number-of-cars-residence
0
1000
500.0
1
1
NIL
HORIZONTAL

BUTTON
55
48
118
81
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
23
126
195
159
max-station-road
max-station-road
3
10
10.0
1
1
NIL
HORIZONTAL

SLIDER
22
168
194
201
average-speed
average-speed
50
90
90.0
1
1
NIL
HORIZONTAL

MONITOR
1141
11
1198
56
Sport
count cars with [typeOfCar = \"sport\"]
17
1
11

MONITOR
1142
102
1199
147
PickUP
count cars with [typeOfCar = \"pickup\"]
17
1
11

MONITOR
1142
57
1199
102
Small
count cars with [typeOfCar = \"small\"]
17
1
11

MONITOR
1204
10
1297
55
HomeCharging
count cars with [preference = 0]
17
1
11

MONITOR
1204
58
1306
103
StationCharging
count cars with [preference = 1]
17
1
11

MONITOR
1139
367
1309
412
NIL
total-watts-home
17
1
11

PLOT
1141
149
1475
367
Compare Watts
ticks
watts
0.0
1000.0
0.0
1000.0
true
true
"" ""
PENS
"total-watts-home" 1.0 0 -16777216 true "" "plot total-watts-home"
"total-watts-stations" 1.0 0 -13840069 true "" "plot total-watts-stations"

MONITOR
1310
366
1472
411
NIL
total-watts-stations
17
1
11

MONITOR
1140
420
1276
465
HOME 0
[watts] of residence 0
17
1
11

MONITOR
1284
420
1410
465
HOME 1
[watts] of residence 1
17
1
11

MONITOR
1140
464
1277
509
HOME 2
[watts] of residence 2
17
1
11

MONITOR
1283
466
1408
511
HOME 3
[watts] of residence 3
17
1
11

SLIDER
23
207
195
240
probability-of-waiting
probability-of-waiting
0
1
1.0
0.01
1
NIL
HORIZONTAL

INPUTBOX
25
246
103
306
waiting-time
40.0
1
0
Number

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="7200"/>
    <metric>total-watts-home</metric>
    <metric>total-watts-stations</metric>
    <metric>sum [km] of cars with[ preference = 0]</metric>
    <metric>sum [km] of cars with[ preference = 1]</metric>
    <enumeratedValueSet variable="max-station-road">
      <value value="10"/>
    </enumeratedValueSet>
    <steppedValueSet variable="probability-of-waiting" first="0" step="0.05" last="1"/>
    <enumeratedValueSet variable="average-speed">
      <value value="90"/>
    </enumeratedValueSet>
    <steppedValueSet variable="waiting-time" first="10" step="10" last="60"/>
    <enumeratedValueSet variable="number-of-cars-residence">
      <value value="500"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
